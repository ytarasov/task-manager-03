# TASK MANAGER

## DEVELOPER

**Name**: Юрий Тарасов

**E-mail**: yuriy.tarasov.96@mail.ru

## SOFTWARE

Java: JDK 1.8

OS: Windows 10

## HARDWARE

CPU: Core i5-9400T

RAM: 16Gb

SSD: 235Gb

## APPLICATION RUN

```
java -jar task-manager.jar
```
